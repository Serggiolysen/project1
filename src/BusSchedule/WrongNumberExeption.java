package BusSchedule;

public class WrongNumberExeption extends IndexOutOfBoundsException {
    public WrongNumberExeption(String message) {
        super(message);
    }
}
