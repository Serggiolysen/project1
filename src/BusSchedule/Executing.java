package BusSchedule;

import BusSchedule.Main_menu.LocaleMenu;

import java.io.IOException;
import java.time.Duration;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.*;

public class Executing {
    private static final Logger LOGGER = Logger.getLogger(Executing.class.getName());
    FileHandler fileHandler;

    {
        try {
            fileHandler = new FileHandler("src\\BusSchedule\\logging\\Shedule.log");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    LocaleMenu localeMenu = new LocaleMenu();

    private static final Scanner SCAN = new Scanner(System.in);

    public SheduleCreation sheduleCreation = new SheduleCreation();

    public void sortingById() {
        System.out.println("\n"+localeMenu.getBundle().getString("Sorting_by_shedule_ID")+":");
        sheduleCreation.getSheduleCreations().sort(Shedule.compareByID);
    }

    public void sortingByEndLocation() {
        System.out.println("\n"+localeMenu.getBundle().getString("Sorting_by_end_Location")+":");
        sheduleCreation.getSheduleCreations().sort(new Comparator<Shedule>() {
            @Override
            public int compare(Shedule o1, Shedule o2) {
                return o1.getLocationEnd().compareTo(o2.getLocationEnd());
            }
        });
    }

    public void searchById() {
        System.out.println(localeMenu.getBundle().getString("Enter_the_shedule_ID_for_searching")+":");
        try {
            int st = SCAN.nextInt();
        for (Shedule i : sheduleCreation.getSheduleCreations()) {
            if (i.getSheduleID() == st) {
                System.out.println(localeMenu.getBundle().getString("Shedule_ID_is")+ ": " + st + "  " + localeMenu.getBundle().getString("Bus number=") + i.getBusNumber() + "   " + localeMenu.getBundle().getString("Price=") + i.getPrice()
                        + "   " + localeMenu.getBundle().getString("Location start=") + i.getLocationStart() + "   " + localeMenu.getBundle().getString("End location=") + i.getLocationEnd() + "   "
                        + localeMenu.getBundle().getString("Start date=") + i.getDateStart().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                        + "   " + localeMenu.getBundle().getString("End date=") + i.getDateEnd().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                        + "   " + localeMenu.getBundle().getString("Waypoints=") + i.getWaypoints() + "   ");
            }
        }
        }catch (InputMismatchException | ArrayIndexOutOfBoundsException e){
            try {
                LOGGER.addHandler(new FileHandler("src\\BusSchedule\\logging\\Shedule.log"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            LOGGER.log(Level.INFO, e.getMessage());
            for (Handler h : LOGGER.getHandlers()) {
                h.close();
            }
            System.out.print(localeMenu.getBundle().getString("Wrong_shedule_ID,_try_one_more_time"));
        }
    }

    public void searchByLocationEnd() {

        System.out.print(localeMenu.getBundle().getString("Enter_the_end_location_for_searching")+":");
        try {
             String st = SCAN.nextLine();
        for (Shedule i : sheduleCreation.getSheduleCreations()) {
            if (i.getLocationEnd().equals(st)) {
                System.out.println(localeMenu.getBundle().getString("End_location_is") + ": " + st + "  " + localeMenu.getBundle().getString("SeduleID=") + i.getSheduleID() + "  " + localeMenu.getBundle().getString("Bus number=") + i.getBusNumber()
                        + "   " + localeMenu.getBundle().getString("Price=") + i.getPrice() + "   " + localeMenu.getBundle().getString("Start location=") + i.getLocationStart() + "   " + localeMenu.getBundle().getString("Start date=")
                        + i.getDateStart().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                        + "   " + localeMenu.getBundle().getString("End date=") + i.getDateEnd().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                        + "   " + localeMenu.getBundle().getString("Waypoints=") + i.getWaypoints() + "   ");
            }
        }
        }catch (InputMismatchException  e){
            try {
                LOGGER.addHandler(new FileHandler("src\\BusSchedule\\logging\\Shedule.log"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            LOGGER.log(Level.INFO, e.getMessage());
            for (Handler h : LOGGER.getHandlers()) {
                h.close();
            }
            System.out.print(localeMenu.getBundle().getString("Wrong_shedule_destination,_try_one_more_time"));
        }
    }

    public void sortingByPrice() {
        System.out.println("\n"+localeMenu.getBundle().getString("Sorting_by_price")+":");
        sheduleCreation.getSheduleCreations().sort(Comparator.comparingInt(Shedule::getPrice));
    }

    public void sortingByDate() {
        System.out.println("\n"+localeMenu.getBundle().getString("Sorting_by_Start_date")+":");
        sheduleCreation.getSheduleCreations().sort(new Comparator<Shedule>() {
            @Override
            public int compare(Shedule o1, Shedule o2) {
                return o1.getDateStart().compareTo(o2.getDateStart());
            }
        });
    }

    public void out() {
        for (Shedule i : sheduleCreation.getSheduleCreations())
            System.out.print(i);
    }

    public void defaultSheduleOut() {
        StringBuilder sb = new StringBuilder();
        sb.append(localeMenu.getBundle().getString("Default_shedule")+"\n");
        for (Shedule i : sheduleCreation.getSheduleCreations())
            sb.append(i);
        sb.append("--------------------------------------------------------------------------------");
        System.out.println(sb);
    }

    public void sortingByWayTime() {
        System.out.println("\n" +localeMenu.getBundle().getString("Sorting_by_way_time")+":");
        sheduleCreation.getSheduleCreations().sort(new Comparator<Shedule>() {
            @Override
            public int compare(Shedule o1, Shedule o2) {
                return ((int) Duration.between(o1.getDateStart(), o1.getDateEnd()).getSeconds()) - ((int) Duration.between(o2.getDateStart(), o2.getDateEnd()).getSeconds());
            }
        });
        List tmpDuration = new ArrayList<>();
        for (Shedule i : sheduleCreation.getSheduleCreations()) {
            long durationInSec = Duration.between(i.getDateStart(), i.getDateEnd()).getSeconds();
            int durationInH = (int) (durationInSec / 3600);
            int durationInMin = (int) ((durationInSec - durationInH * 3600) / 60);
            String resultTemeString = durationInH + " "+ localeMenu.getBundle().getString("hours")+
                    " : " + durationInMin + " "+ localeMenu.getBundle().getString("minutes");
            System.out.print(resultTemeString + "    " + i);
        }
    }

}
