package BusSchedule.Sub_Menu;

import BusSchedule.Executing;
import BusSchedule.Interfaces.SubSubMenu;
import BusSchedule.Main_menu.LocaleMenu;
import BusSchedule.Main_menu.MainMenu_Display;

public class SubMenu_SearchSheduleNumber implements SubSubMenu {
    LocaleMenu localeMenu = new LocaleMenu();

    @Override
    public String name() {
        return localeMenu.getBundle().getString("Search_by_sedule_ID");
    }

    @Override
    public void execute() {
        Executing exe = new Executing();
        exe.defaultSheduleOut();
        exe.searchById();
        new MainMenu_Display().select();
    }
}
