package BusSchedule.Sub_Menu;

import BusSchedule.Executing;
import BusSchedule.Interfaces.SubSubMenu;
import BusSchedule.Main_menu.LocaleMenu;
import BusSchedule.Main_menu.MainMenu_Display;

public class SubMenu_SortShedulePrice implements SubSubMenu {
    LocaleMenu localeMenu = new LocaleMenu();

    @Override
    public String name() {
        return localeMenu.getBundle().getString("Sort_by_shedule_price");
    }

    @Override
    public void execute() {
        Executing exe = new Executing();
        exe.defaultSheduleOut();
        exe.sortingByPrice();
        exe.out();
        new MainMenu_Display().select();
    }


}
