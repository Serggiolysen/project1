package BusSchedule.Main_menu;

import BusSchedule.Interfaces.MainMenu;
import BusSchedule.WrongNumberExeption;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.*;

public class MainMenu_Display implements MainMenu {
    private static final Logger LOGGER = Logger.getLogger(MainMenu_Display.class.getName());
    LocaleMenu localeMenu = new LocaleMenu();
    private static final Scanner SCANNER = new Scanner(System.in);

    private MainMenu[] mainMenus = {new MainMenu_SheduleInformation(), new MainMenu_SheduleSearch(), new MainMenu_SheduleSort()};

    public void select() {
        System.out.println(name());
        for (int i = 0; i < mainMenus.length; i++) {
            System.out.println(i + " " + mainMenus[i].name());
        }
        System.out.println(localeMenu.getBundle().getString("Enter_menu_number"));
        try {
            for (int i = 0; i < mainMenus.length; i++) {
                i = SCANNER.nextInt();
                mainMenus[i].name();
                mainMenus[i].select();
            }
        } catch (WrongNumberExeption | ArrayIndexOutOfBoundsException e) {
            try {
                LOGGER.addHandler(new FileHandler("src\\BusSchedule\\logging\\Shedule.log"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            LOGGER.log(Level.INFO, e.getMessage());
            for (Handler h : LOGGER.getHandlers()) {
                h.close();
            }
            System.out.println(localeMenu.getBundle().getString("Wrong_menu_number"));
            select();
        }
    }

    @Override
    public String name() {
        return "\n"+localeMenu.getBundle().getString("Main_menu");
    }
}
