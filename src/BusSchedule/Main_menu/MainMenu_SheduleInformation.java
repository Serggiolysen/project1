package BusSchedule.Main_menu;

import BusSchedule.Interfaces.*;
import BusSchedule.Sub_Menu.*;
import BusSchedule.WrongNumberExeption;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.*;

public class MainMenu_SheduleInformation implements MainMenu {
    private static final Logger LOGGER = Logger.getLogger(MainMenu_SheduleInformation.class.getName());
    LocaleMenu localeMenu = new LocaleMenu();

    private static final Scanner SCANNER = new Scanner(System.in);

    private SubSubMenu[] subMenus = {new SubMenu_SortSheduleNumber(), new SubMenu_SortSheduleDestination()};

    @Override
    public String name() {
        return localeMenu.getBundle().getString("Shedule_Information");
    }

    @Override
    public void select() {
        for (int i = 0; i < subMenus.length; i++) {
            System.out.println(i + " " + subMenus[i].name());
        }
        System.out.println(localeMenu.getBundle().getString("Enter_submenu_number"));
        try{
            for (int i = 0; i < subMenus.length; i++) {
                i = SCANNER.nextInt();
                subMenus[i].execute();
            }
        }catch (WrongNumberExeption | ArrayIndexOutOfBoundsException e){
            try {
                LOGGER.addHandler(new FileHandler("src\\BusSchedule\\logging\\Shedule.log"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            LOGGER.log(Level.INFO, e.getMessage());
            for (Handler h : LOGGER.getHandlers()) {
                h.close();
            }
            System.out.println(localeMenu.getBundle().getString("Wrong_submenu_number"));
            try {
                Thread.sleep(200);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            select();
        }
    }
}
