package BusSchedule.Main_menu;

import BusSchedule.Interfaces.*;
import BusSchedule.Sub_Menu.*;
import BusSchedule.WrongNumberExeption;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainMenu_SheduleSearch implements MainMenu {
    private static final Logger LOGGER = Logger.getLogger(MainMenu_SheduleSearch.class.getName());
    LocaleMenu localeMenu = new LocaleMenu();
    private static final Scanner SCANNER = new Scanner(System.in);

    private SubSubMenu[] subMenus = {new SubMenu_SearchSheduleNumber(), new SubMenu_SearchSheduleDestination()};

    @Override
    public String name() {
        return localeMenu.getBundle().getString("Shedule_search");
    }

    @Override
    public void select() {
        for (int i = 0; i < subMenus.length; i++) {
            System.out.println(i + " " + subMenus[i].name());
        }
        System.out.println(localeMenu.getBundle().getString("Enter_submenu_number")+"\n");
        try{
            for (int i = 0; i < subMenus.length; i++) {
                i = SCANNER.nextInt();
                subMenus[i].execute();
            }
        }catch (WrongNumberExeption | ArrayIndexOutOfBoundsException e){
            LOGGER.log(Level.INFO, e.getMessage(), e);
            System.out.println(localeMenu.getBundle().getString("Wrong_submenu_number"));
            try {
                Thread.sleep(200);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            select();
        }
    }
}