package BusSchedule.Main_menu;

import java.util.*;

public class LocaleMenu {

    private Scanner SCANNER = new Scanner(System.in);
    static ResourceBundle bundle;
    private List<Locale> locales = Arrays.asList(new Locale("ru"), new Locale("en"));

    public void localesShow() {
        StringBuilder showLocale = new StringBuilder("Какой язык:\n");
        for (int i = 0; i < locales.size(); i++) {
            showLocale.append(i).append(". ").append(locales.get(i)).append('\n');
        }
        System.out.print(showLocale);
        int choice = SCANNER.nextInt();
        bundle = ResourceBundle.getBundle("BusSchedule\\locale", locales.get(choice));
    }

    public ResourceBundle getBundle() {
        return bundle;
    }
}
