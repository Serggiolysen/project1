package BusSchedule;

import BusSchedule.Main_menu.LocaleMenu;
import BusSchedule.Main_menu.MainMenu_Display;

public class Main extends LocaleMenu {

    public static void main(String someArgs[]) {
        LocaleMenu localeMenu = new LocaleMenu();
        localeMenu.localesShow();

        MainMenu_Display menuDisplay = new MainMenu_Display();
        menuDisplay.select();

    }
}
