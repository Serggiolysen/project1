package BusSchedule;

import BusSchedule.Main_menu.LocaleMenu;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Shedule implements Comparable<Shedule> {
    private int sheduleID;
    private int busNumber;
    private int price;
    private String LocationStart;
    private String LocationEnd;
    private LocalDateTime dateStart;
    private LocalDateTime dateEnd;
    private String waypoints;

    LocaleMenu localeMenu = new LocaleMenu();

    public Shedule(int sheduleID, int busNumber, int price, String locationStart, String locationEnd, LocalDateTime dateStart, LocalDateTime dateEnd, String waypoints) {
        this.sheduleID = sheduleID;
        this.busNumber = busNumber;
        this.price = price;
        LocationStart = locationStart;
        LocationEnd = locationEnd;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.waypoints = waypoints;
    }

    public int getSheduleID() {
        return sheduleID;
    }

    public int getBusNumber() {
        return busNumber;
    }

    public int getPrice() {
        return price;
    }

    public String getLocationStart() {
        return LocationStart;
    }

    public String getLocationEnd() {
        return LocationEnd;
    }

    public LocalDateTime getDateStart() {
        return dateStart;
    }

    public LocalDateTime getDateEnd() {
        return dateEnd;
    }

    public String getWaypoints() {
        return waypoints;
    }

    @Override
    public String toString() {
        return String.format("|%s|%n", localeMenu.getBundle().getString("ID=") + this.sheduleID + "  " + localeMenu.getBundle().getString("Bus number=") + this.busNumber + "     " + localeMenu.getBundle().getString("Price=") + this.price + "     " + localeMenu.getBundle().getString("Location start=") + this.LocationStart
                + "    " + localeMenu.getBundle().getString("Location end=") + this.LocationEnd + "   " + localeMenu.getBundle().getString("Date start=") + this.dateStart.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                + "   " + localeMenu.getBundle().getString("Date end=") + this.dateEnd.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "   " + localeMenu.getBundle().getString("Waypoints=") + this.waypoints);

    }

    @Override
    public int compareTo(Shedule o) {
        return this.price - o.getPrice();
    }

    public static Comparator<Shedule> compareByID = Comparator.comparingInt(Shedule::getSheduleID);

}
