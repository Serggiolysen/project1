package BusSchedule.Interfaces;

public interface SubMenu {

    String name();

    void search();

    void sort();

}
