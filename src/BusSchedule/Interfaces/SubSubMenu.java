package BusSchedule.Interfaces;

public interface SubSubMenu {
    String name();

    void execute();

}
