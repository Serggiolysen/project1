package BusSchedule;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class SheduleCreation  {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss Z");
    private List<Shedule> sheduleCreations = new ArrayList<>();

    {
        sheduleCreations.add(new Shedule(0, 45, 300, "Minsk", "Brest", LocalDateTime.parse("2015-04-25 12:35:00 +0300", FORMATTER), LocalDateTime.parse("2015-04-25 17:35:00 +0300", FORMATTER), "Baranovichi BY"));
        sheduleCreations.add(new Shedule(1, 10, 5329, "Minsk", "Berlin", LocalDateTime.parse("2015-05-01 12:35:00 +0300", FORMATTER), LocalDateTime.parse("2015-05-02 17:35:00 +0200", FORMATTER), "Brest BY,  Warsaw PL, Wroclaw PL"));
        sheduleCreations.add(new Shedule(5, 234, 2134, "Minsk", "Moscow", LocalDateTime.parse("2015-05-01 12:54:00 +0300", FORMATTER), LocalDateTime.parse("2015-05-02 20:50:00 +0300", FORMATTER), "Orsha BY"));
        sheduleCreations.add(new Shedule(2, 356, 1244, "Minsk", "Grodno", LocalDateTime.parse("2015-03-10 22:00:00 +0300", FORMATTER), LocalDateTime.parse("2015-03-10 23:35:00 +0300", FORMATTER), "Lida BY"));
        sheduleCreations.add(new Shedule(3, 15, 124, "Minsk", "Vitebsk", LocalDateTime.parse("2015-05-12 15:00:00 +0300", FORMATTER), LocalDateTime.parse("2015-05-12 18:30:00 +0300", FORMATTER), "Baranovichi BY"));
        sheduleCreations.add(new Shedule(4, 234, 23534, "Minsk", "Gomel", LocalDateTime.parse("2015-03-02 14:20:00 +0300", FORMATTER), LocalDateTime.parse("2015-03-02 17:35:00 +0300", FORMATTER), "Mogilev BY"));
    }

    public List<Shedule> getSheduleCreations() {
        return sheduleCreations;
    }
}



